# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from .test_sale_process_shipment_done import suite

__all__ = ['suite']
