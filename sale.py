# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.pool import PoolMeta, Pool
from trytond.pyson import Eval, Id
from trytond.wizard import Wizard, StateTransition, StateView, Button
from trytond.transaction import Transaction

__all__ = ['Sale', 'ProcessTry']


class Sale(metaclass=PoolMeta):
    __name__ = 'sale.sale'

    do_shipment = fields.Boolean('Do shipment',
        states={'readonly': Eval('state').in_([
            'processing', 'done'])})

    @classmethod
    def __setup__(cls):
        super(Sale, cls).__setup__()
        cls._buttons.update({
            'process_try': dict(cls._buttons['process']),
            'process_and_ship': {}
            })
        cls._buttons['process']['invisible'] = True

    @classmethod
    @ModelView.button_action(
        'sale_process_shipment_done.wizard_sale_process_try')
    def process_try(cls, sales):
        pass

    @staticmethod
    def default_do_shipment():
        Conf = Pool().get('sale.configuration')
        return Conf(1).do_shipment

    def _get_shipment_sale(self, Shipment, key):
        shipment = super(Sale, self)._get_shipment_sale(Shipment, key)
        if self.do_shipment:
            shipment.effective_date = self.sale_date
        return shipment

    @classmethod
    @ModelView.button
    def process_and_ship(cls, sales):
        pool = Pool()
        Shipment = pool.get('stock.shipment.out')
        ShipmentReturn = pool.get('stock.shipment.out.return')

        cls.process(sales)
        shipments = [sh for s in sales for sh in s.shipments]
        shipment_returns = [sh for s in sales for sh in s.shipment_returns]
        Shipment.assign_force(shipments)
        Shipment.pack(shipments)
        Shipment.done(shipments)
        ShipmentReturn.receive(shipment_returns)


class FailedStateView(StateView):

    @property
    def shipments(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        id_sale = Transaction().context.get('active_id')
        return Sale(id_sale).shipments if id_sale else []

    def get_view(self, wizard, state_name):
        with Transaction().set_context(
                active_id=self.shipments[0].id if self.shipments else None):
            return super(FailedStateView, self).get_view(
                wizard, state_name)

    def get_defaults(self, wizard, state_name, fields):
        with Transaction().set_context(
                active_id=self.shipments[0].id if self.shipments else None):
            return super(FailedStateView, self).get_defaults(
                wizard, state_name, fields)


class ProcessTry(Wizard):
    '''Process Try Wizard'''
    __name__ = 'sale.process_try'

    start = StateTransition()
    failed = FailedStateView('stock.shipment.out.assign.failed',
        'stock.shipment_out_assign_failed_view_form', [
            Button('Force Assign', 'force', 'tryton-forward',
                states={
                    'invisible': ~Id('stock',
                        'group_stock_force_assignment').in_(
                        Eval('context', {}).get('groups', [])),
                    }),
            Button('OK', 'revert', 'tryton-ok', True),
            ])
    revert = StateTransition()
    force = StateTransition()

    @property
    def sale(self):
        pool = Pool()
        Sale = pool.get('sale.sale')

        return Sale(Transaction().context['active_id'])

    def transition_start(self):
        pool = Pool()
        Sale = pool.get('sale.sale')
        Shipment = pool.get('stock.shipment.out')

        sale = self.sale
        if not sale.do_shipment:
            # process sale and exit
            Sale.process([sale])
            return 'end'
        sale.create_invoice()
        sale.set_invoice_state()
        shipments = sale.create_shipment('out')
        shipments_return = sale.create_shipment('return')
        sale.set_shipment_state()
        if not shipments and shipments_return:
            return 'force'
        if shipments and Shipment.assign_try(shipments):
            return 'force'
        else:
            return 'failed'

    def transition_revert(self):
        pool = Pool()
        Move = pool.get('stock.move')
        Shipment = pool.get('stock.shipment.out')
        ShipmentReturn = pool.get('stock.shipment.out.return')
        Invoice = pool.get('account.invoice')
        InvoiceLine = pool.get('account.invoice.line')

        sale = self.sale
        invoice_lines = [invoice_line for line in sale.lines
            for invoice_line in line.invoice_lines]
        invoices = set([l.invoice for l in invoice_lines if l.invoice])
        InvoiceLine.write(invoice_lines, {'origin': None})
        invoice_lines = InvoiceLine.browse([l.id for l in invoice_lines])
        InvoiceLine.delete(invoice_lines)

        # consider grouping
        invoice_lines = InvoiceLine.search([
            ('invoice', 'in', [i.id for i in invoices])])
        for line in invoice_lines:
            if line.invoice in invoices:
                invoices.remove(line.invoice)
        if invoices:
            Invoice.delete(invoices)

        shipments = list(sale.shipments)
        shipment_returns = list(sale.shipment_returns)
        moves = [move for shipment in (sale.shipments + sale.shipment_returns)
            for move in shipment.moves]
        Move.write(moves, {'origin': None})
        if shipments:
            Shipment.delete(shipments)
        if shipment_returns:
            ShipmentReturn.delete(shipment_returns)
        return 'end'

    def transition_force(self):
        pool = Pool()
        Sale = pool.get('sale.sale')

        Sale.process_and_ship([self.sale])
        return 'end'
