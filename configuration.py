# The COPYRIGHT file at the top level of this repository contains the full
# copyright notices and license terms.
from trytond.model import fields, ModelSQL
from trytond.pool import PoolMeta
from trytond.modules.company.model import CompanyValueMixin
from trytond.modules.sale.configuration import default_func

__all__ = ['Configuration', 'ConfigurationDoShipment']


class Configuration(metaclass=PoolMeta):
    __name__ = 'sale.configuration'

    do_shipment = fields.MultiValue(fields.Boolean('Do shipment',
        help='Do the shipment when processing Sale.'))

    default_do_shipment = default_func('do_shipment')


class ConfigurationDoShipment(ModelSQL, CompanyValueMixin):
    """Sale Configuration Do shipment"""
    __name__ = 'sale.configuration.do_shipment'

    do_shipment = fields.Boolean('Do shipment')

    @staticmethod
    def default_do_shipment():
        return True
