# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from . import sale
from . import configuration


def register():
    Pool.register(
        sale.Sale,
        configuration.Configuration,
        configuration.ConfigurationDoShipment,
        module='sale_process_shipment_done', type_='model')
    Pool.register(
        sale.ProcessTry,
        module='sale_process_shipment_done', type_='wizard')
    Pool.register(
        module='sale_process_shipment_done', type_='report')
